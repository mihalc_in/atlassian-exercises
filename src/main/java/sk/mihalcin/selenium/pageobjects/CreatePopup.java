package sk.mihalcin.selenium.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by pmihalcin on 25. 7. 2015.
 */
public class CreatePopup {

    private static final Logger LOG = LoggerFactory.getLogger(CreatePopup.class);

    private final WebDriver driver;

    @FindBy(className = "templates")
    private WebElement templatePopup;
    private final By templateClass = By.className("template-name");
    @FindBy(xpath = "//*[@id=\"create-dialog\"]")
    private WebElement createDialog;
    private final By createPageButtonPath = By.xpath("//*[@id=\"create-dialog\"]/div/div[2]/button");

    public CreatePopup(WebDriver driver) {
        this.driver = driver;

        LOG.debug("Page title = " + driver.getTitle());

        if (!"Dashboard - Confluence".equalsIgnoreCase(driver.getTitle())) {
            driver.quit();
            throw new IllegalStateException("This is not the create popup");
        }
    }

    private CreatePopup selectBlankPageTemplate() {
        List<WebElement> templateNames = templatePopup.findElements(templateClass);

        LOG.debug("Templates found:");
        templateNames.stream().map(WebElement::getText).reduce((s1, s2) -> s1 + " | " + s2).ifPresent(LOG::debug);

        LOG.debug("Selecting blank page template...");
        for (WebElement template : templateNames) {
            if ("Blank page".equalsIgnoreCase(template.getText())) {
                template.click();
            }
        }
        return this;
    }

    private CreatePopup clickCreatePage() {
        WebElement createPageButton = createDialog.findElement(createPageButtonPath);
        createPageButton.click();
        return this;
    }

    public NewPage createPage() {
        selectBlankPageTemplate();
        LOG.debug("Creating new page...");
        clickCreatePage();
        return PageFactory.initElements(driver, NewPage.class);
    }

}
