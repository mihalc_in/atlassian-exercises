package sk.mihalcin.selenium.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by pmihalcin on 25. 7. 2015.
 */
public class LoginPage {

    private static final Logger LOG = LoggerFactory.getLogger(LoginPage.class);

    private final WebDriver driver;

    @FindBy(name = "username")
    private WebElement usernameField;
    @FindBy(name = "password")
    private WebElement passwordField;
    @FindBy(id = "login")
    private WebElement loginButton;

    public LoginPage(WebDriver driver) {
        this.driver = driver;

        LOG.debug("Page title = " + driver.getTitle());
        if (!"Atlassian Cloud".equalsIgnoreCase(driver.getTitle())) {
            driver.quit();
            throw new IllegalStateException("This is not the login page");
        }
    }

    private LoginPage typeUsername(String username) {
        usernameField.sendKeys(username);
        return this;
    }

    private LoginPage typePassword(String password) {
        passwordField.sendKeys(password);
        return this;
    }

    private MainPage submitLogin() {
        loginButton.submit();
        return PageFactory.initElements(driver, MainPage.class);
    }

    public MainPage loginAs(String username, String password) {
        typeUsername(username);
        typePassword(password);
        LOG.debug("Logging in user " + username);
        return submitLogin();
    }

    public static LoginPage navigateTo(String url, WebDriver driver) {
        driver.get(url);
        return PageFactory.initElements(driver, LoginPage.class);
    }
}
