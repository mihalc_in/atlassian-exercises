package sk.mihalcin.selenium.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

/**
 * Created by pmihalcin on 25. 7. 2015.
 */
public class NewPage {
    private static final Logger LOG = LoggerFactory.getLogger(NewPage.class);

    private final WebDriver driver;
    @FindBy(name = "title")
    private WebElement titleInput;
    @FindBy(id = "savebar-container")
    private WebElement saveBar;
    private final By confirmButtonLocator = By.name("confirm");
    private String name = "";

    public NewPage(WebDriver driver) {
        this.driver = driver;

        LOG.debug("Page title = " + driver.getTitle());

        if (!driver.getTitle().startsWith("Add Page")) {
            driver.quit();
            throw new IllegalStateException("This is not the add page");
        }
    }

    private NewPage setUniqueTitle() {
        name = UUID.randomUUID().toString();
        titleInput.sendKeys(name);
        return this;
    }

    private NewPage clickSavePage() {
        WebElement saveButton = saveBar.findElement(confirmButtonLocator);
        saveButton.click();
        return this;
    }

    public NewPage savePage() {
        setUniqueTitle();
        LOG.debug("Saving new page...");
        clickSavePage();
        return this;
    }

    public String getName() {
        return name;
    }
}
