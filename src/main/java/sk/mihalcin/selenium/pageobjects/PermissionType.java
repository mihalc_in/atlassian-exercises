package sk.mihalcin.selenium.pageobjects;

/**
 * Created by pmihalcin on 26. 7. 2015.
 */
public enum PermissionType {
    NONE("No restrictions"),
    EDIT("Edit restrictions"),
    VIEW_EDIT("View and edit restrictions");

    private final String description;

    private PermissionType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
