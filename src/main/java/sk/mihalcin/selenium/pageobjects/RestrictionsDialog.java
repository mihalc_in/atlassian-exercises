package sk.mihalcin.selenium.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by pmihalcin on 26. 7. 2015.
 */
public class RestrictionsDialog {
    private static final Logger LOG = LoggerFactory.getLogger(RestrictionsDialog.class);

    private final WebDriver driver;
    @FindBy(id = "s2id_page-restrictions-dialog-selector")
    private WebElement dropDown;
    @FindBy(xpath = "//*[@id=\"update-page-restrictions-dialog\"]/div/div[2]/table/tbody/tr")
    private List<WebElement> tableRows;
    @FindBy(xpath = "//*[@id=\"page-restrictions-dialog-close-button\"]")
    private WebElement cancelButton;
    @FindBy(xpath = "//*[@id=\"page-restrictions-add-button\"]")
    private WebElement addButton;
    @FindBy(xpath = "//*[@id=\"page-restrictions-dialog-save-button\"]")
    private WebElement applyButton;
    @FindBy(xpath = "//*[@id=\"s2id_restrictions-dialog-auto-picker\"]/ul/li")
    private WebElement searchField;
    private List<WebElement> options;

    public RestrictionsDialog(WebDriver driver) {
        this.driver = driver;

        By dialogHeaderLocator = By.xpath("//*[@id=\"update-page-restrictions-dialog\"]/header/h2");
        String headerText = driver.findElement(dialogHeaderLocator).getText();
        LOG.debug("Dialog title = " + headerText);

        if (!headerText.equalsIgnoreCase("Restrictions")) {
            driver.quit();
            throw new IllegalStateException("This is not the restrictions dialog");
        }
    }

    public void addPermissions(String usernameOrGroup, PermissionType permissionType) {
        WebElement dropDownLink = dropDown.findElement(By.tagName("a"));
        dropDownLink.click();

        loadOptions();

        for (WebElement option : options) {
            if (option.getText().startsWith(permissionType.getDescription())) {
                option.click();
                break;
            }
        }

        // search for given user name and group
        WebElement input = searchField.findElement(By.tagName("input"));
        input.sendKeys(usernameOrGroup);
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        input.sendKeys(Keys.TAB);

        // add given user name or group
        addButton.click();

        // apply changes
        applyButton.click();
    }

    public void setNoRestrictions() {
        LOG.debug("Setting no restrictions...");

        WebElement dropDownLink = dropDown.findElement(By.tagName("a"));
        dropDownLink.click();
        loadOptions();
        options.get(0).click();
        loadApplyButton();

        // if apply button is not enabled, loop through all options to enable apply button
        if (!applyButton.isEnabled()) {
            int i = options.size();

            while (i-- > 0) {
                dropDownLink.click();
                loadOptions();
                options.get(i).click();
                loadApplyButton();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        applyButton.click();
    }

    private void loadOptions() {
        WebElement dropDownList = driver.findElement(By.xpath("//*[@id=\"select2-drop\"]/ul"));
        options = dropDownList.findElements(By.tagName("li"));
    }

    private void loadApplyButton() {
        applyButton = driver.findElement(By.xpath("//*[@id=\"page-restrictions-dialog-save-button\"]"));
    }

    public boolean checkPermissions(String usernameOrGroup, PermissionType permissionType) {
        WebElement dropDownLink = dropDown.findElement(By.tagName("a"));

        dropDownLink.click();
        loadOptions();

        // find given option
        for (WebElement option : options) {
            if (option.getText().startsWith(permissionType.getDescription())) {
                option.click();
                break;
            }
        }

        boolean found = false;
        LOG.debug("Trying to find username or group " + usernameOrGroup);

        outerLoop:
        for (WebElement tableRow : tableRows) {
            List<WebElement> tds = tableRow.findElements(By.xpath("td"));
            for (WebElement td : tds) {
                if (td.getText().equalsIgnoreCase(usernameOrGroup)) {
                    LOG.debug(usernameOrGroup + " has been successfully found");
                    found = true;
                    break outerLoop;
                }
            }
        }

        // close the dialog
        cancelButton.click();
        return found;
    }
}
