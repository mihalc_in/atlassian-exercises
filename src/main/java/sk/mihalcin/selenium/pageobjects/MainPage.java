package sk.mihalcin.selenium.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by pmihalcin on 25. 7. 2015.
 */
public class MainPage {

    private static final Logger LOG = LoggerFactory.getLogger(MainPage.class);

    private final WebDriver driver;
    @FindBy(id = "create-page-button")
    private WebElement createButton;
    @FindBy(className = "space-name")
    private WebElement spaceLink;

    public MainPage(WebDriver driver) {
        this.driver = driver;

        LOG.debug("Page title = " + driver.getTitle());

        if (!"Dashboard - Confluence".equalsIgnoreCase(driver.getTitle())) {
            driver.quit();
            throw new IllegalStateException("This is not the main page");
        }
    }

    public CreatePopup createPage() {
        LOG.debug("Creating new page...");
        createButton.click();
        return PageFactory.initElements(driver, CreatePopup.class);
    }

    public SpacePage goToSpacePage() {
        String spaceName = spaceLink.getText();
        LOG.debug("Navigating to space: " + spaceName);
        spaceLink.click();
        return PageFactory.initElements(driver, SpacePage.class);
    }

}
