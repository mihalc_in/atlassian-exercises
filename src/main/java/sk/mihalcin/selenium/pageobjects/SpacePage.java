package sk.mihalcin.selenium.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by pmihalcin on 26. 7. 2015.
 */
public class SpacePage {

    private static final Logger LOG = LoggerFactory.getLogger(SpacePage.class);

    private final WebDriver driver;
    @FindBy(xpath = "//*[@id=\"action-menu-link\"]")
    private WebElement actionMenuLink;
    @FindBy(xpath = "//*[@id=\"action-page-permissions-link\"]")
    private WebElement restrictionsLink;

    public SpacePage(WebDriver driver) {
        this.driver = driver;

        LOG.debug("Page title = " + driver.getTitle());

        if (!driver.getTitle().contains("Space")) {
            driver.quit();
            throw new IllegalStateException("This is not the space page");
        }
    }

    public RestrictionsDialog openRestrictionsPopup() {
        // refresh here
        driver.navigate().refresh();

        LOG.debug("Opening Restrictions...");

        LOG.trace("actionMenuLink.isDisplayed() = " + actionMenuLink.isDisplayed());
        actionMenuLink.click();

        int i = 10;
        // restrictionsLink is sometimes not displayed, so try to open it up to 10 times
        while (i-- > 0) {
            LOG.trace("restrictionsLink.isDisplayed() = " + restrictionsLink.isDisplayed());
            if (restrictionsLink.isDisplayed()) {
                break;
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            actionMenuLink.click();
        }

        restrictionsLink.click();
        return PageFactory.initElements(driver, RestrictionsDialog.class);
    }

}
