package sk.mihalcin.selenium;

import junit.framework.TestCase;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sk.mihalcin.selenium.pageobjects.CreatePopup;
import sk.mihalcin.selenium.pageobjects.LoginPage;
import sk.mihalcin.selenium.pageobjects.MainPage;
import sk.mihalcin.selenium.pageobjects.NewPage;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

@RunWith(BlockJUnit4ClassRunner.class)
public class CreateNewPageTest extends TestCase {

    private static final Logger LOG = LoggerFactory.getLogger(CreateNewPageTest.class);
    public static final int TIMEOUT = 3000;
    public static final String EMAIL = "atlassian@atlassian.com";
    public static final String PASSWORD = "atlassian";
    public static final String CHROMEDRIVER_EXE = "c:\\chrome\\chromedriver.exe";
    public static final String CONFLUENCE_URL = "http://pmihalcin.atlassian.net";

    private static ChromeDriverService service;
    private WebDriver driver;

    @BeforeClass
    public static void createAndStartService() {
        service = new ChromeDriverService.Builder()
                .usingDriverExecutable(new File(CHROMEDRIVER_EXE))
                .usingAnyFreePort()
                .build();
        try {
            LOG.debug("Starting ChromeDriverService");
            service.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @AfterClass
    public static void createAndStopService() {
        LOG.debug("Stopping ChromeDriverService");
        service.stop();
    }

    @Before
    public void createDriver() {
        LOG.debug("Creating driver");
        driver = new RemoteWebDriver(service.getUrl(),
                DesiredCapabilities.chrome());
        driver.manage().timeouts().implicitlyWait(TIMEOUT, TimeUnit.SECONDS);
    }

    @After
    public void quitDriver() {
        LOG.debug("Quiting driver");
        driver.quit();
    }

    @Test
    public void testCreateNewPage() {
        LOG.debug("Test scenario: a user can create a new page");
        LoginPage loginPage = LoginPage.navigateTo(CONFLUENCE_URL, driver);
        MainPage mainPage = loginPage.loginAs(EMAIL, PASSWORD);
        CreatePopup createPopup = mainPage.createPage();
        NewPage newPage = createPopup.createPage();
        newPage.savePage();
        assertThat(driver.getTitle(), containsString(newPage.getName()));
    }
}