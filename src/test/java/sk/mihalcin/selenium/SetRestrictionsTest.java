package sk.mihalcin.selenium;

import junit.framework.TestCase;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sk.mihalcin.selenium.pageobjects.*;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@RunWith(BlockJUnit4ClassRunner.class)
public class SetRestrictionsTest extends TestCase {

    private static final Logger LOG = LoggerFactory.getLogger(SetRestrictionsTest.class);
    public static final int TIMEOUT = 3000;
    public static final String EMAIL = "atlassian@atlassian.com";
    public static final String PASSWORD = "atlassian";
    public static final String SYSTEM_ADMINISTRATORS = "system-administrators";
    public static final String CHROMEDRIVER_EXE = "c:\\chrome\\chromedriver.exe";
    public static final String CONFLUENCE_URL = "http://pmihalcin.atlassian.net";

    private static ChromeDriverService service;
    private WebDriver driver;

    @BeforeClass
    public static void createAndStartService() {
        service = new ChromeDriverService.Builder()
                .usingDriverExecutable(new File(CHROMEDRIVER_EXE))
                .usingAnyFreePort()
                .build();
        try {
            LOG.debug("Starting ChromeDriverService");
            service.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @AfterClass
    public static void createAndStopService() {
        LOG.debug("Stopping ChromeDriverService");
        service.stop();
    }

    @Before
    public void createDriver() {
        LOG.debug("Creating driver");
        driver = new RemoteWebDriver(service.getUrl(),
                DesiredCapabilities.chrome());
        driver.manage().timeouts().implicitlyWait(TIMEOUT, TimeUnit.SECONDS);
    }

    @After
    public void quitDriver() {
        LOG.debug("Quiting driver");
        driver.quit();
    }

    @Test
    public void testEditRestrictions() {
        LOG.debug("Test scenario: a user can set restrictions on an existing page | Edit restrictions");

        LoginPage loginPage = LoginPage.navigateTo(CONFLUENCE_URL, driver);
        MainPage mainPage = loginPage.loginAs(EMAIL, PASSWORD);

        SpacePage spacePage = mainPage.goToSpacePage();
        RestrictionsDialog restrictionsDialog = spacePage.openRestrictionsPopup();
        restrictionsDialog.setNoRestrictions();

        restrictionsDialog = spacePage.openRestrictionsPopup();
        restrictionsDialog.addPermissions(SYSTEM_ADMINISTRATORS, PermissionType.EDIT);

        restrictionsDialog = spacePage.openRestrictionsPopup();
        restrictionsDialog.checkPermissions(SYSTEM_ADMINISTRATORS, PermissionType.EDIT);
    }

    @Test
    public void testViewAndEditRestrictions() {
        LOG.debug("Test scenario: a user can set restrictions on an existing page | View and Edit restrictions");

        LoginPage loginPage = LoginPage.navigateTo(CONFLUENCE_URL, driver);
        MainPage mainPage = loginPage.loginAs(EMAIL, PASSWORD);

        SpacePage spacePage = mainPage.goToSpacePage();
        RestrictionsDialog restrictionsDialog = spacePage.openRestrictionsPopup();
        restrictionsDialog.setNoRestrictions();

        restrictionsDialog = spacePage.openRestrictionsPopup();
        restrictionsDialog.addPermissions(SYSTEM_ADMINISTRATORS, PermissionType.VIEW_EDIT);

        restrictionsDialog = spacePage.openRestrictionsPopup();
        boolean checked = restrictionsDialog.checkPermissions(SYSTEM_ADMINISTRATORS, PermissionType.VIEW_EDIT);
        assertThat(checked, equalTo(true));
    }
}