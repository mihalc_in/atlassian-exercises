This repository contains solutions to both technical exercises.
This README documents steps which are necessary to get the solution to **Exercise 1 - Test Automation** up & running.
It also contains the solution to 2nd exercise - **Exercise 2 - Exploratory Testing**. You can also find it under Source -> exploratory-testing where screenshots of bugs were stored.

# Exercise 1 - Test Automation #
### Overview ###
The solution is Maven project written in Java. Famous [PageObjects](https://code.google.com/p/selenium/wiki/PageObjects) design pattern was applied, so that the API about application is separated from API about HTML. Selenium tests can be executed either as JUnit tests or they will be triggered during Maven lifecycle (test phase). Once executed, Chrome driver navigates to Confluence and executes all steps defined in the test.

### How do I get set up? ###
* Download [Chrome driver](https://sites.google.com/a/chromium.org/chromedriver/downloads) and extract zip file
* Set up [Maven](https://maven.apache.org/)
* Change your credentials in CreateNewPageTest & SetRestrictionsTest classes (EMAIL, PASSWORD constants)
* Change the path to Chrome driver executable in CreateNewPageTest & SetRestrictionsTest classes (CHROMEDRIVER_EXE constant) or set environment variable accordingly
* Change the URL to your Confluence instance in CreateNewPageTest & SetRestrictionsTest classes (CONFLUENCE_URL constant)
* Execute Maven goal: **clean test**

# Exercise 2 - Exploratory Testing #
### Confluence Page Tree feature ###

### The approach ###
I am going to test Page Tree Confluence feature, which displays pages in an expandable and collapsible tree. It allows the user to drag & drop pages.
The session will take 30 minutes and artifacts of this exercise will be list of scenarios, potential bugs found & final word.
I will focus on features of page tree (drag & drop, sorting & undoing) and user experience.

### List of scenarios ###
* Sort alphabetically
* Undo sorting
* Collapse / expand 
* Open up parent page and open 'View in hierarchy'
* Move any page by dragging it to a new position in the tree
* Drop a page on top of another page to make one of them a parent
* Move any page under arbitrary tree node
* Exchange parent node with one of its leafs
* Move out leafs from parent page
* Open new tab, load View in hierarchy feature and check changes in previous tab are instantly applied
* Move out one of the leafs of parent page and move parent page under it
* Sort alphabetically every parent page

### Bugs found ###
* Javascript rendering issues after the whole page tree was moved down
* Sorting feature disabled after few drag & drops

### Final word ###
* Feature is of medium-high quality
* It would be good to address bugs which were found